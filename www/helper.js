/*global cordova, module*/

module.exports = {
    sendToBack: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Helper", "sendToBack", [name]);
    }
};
