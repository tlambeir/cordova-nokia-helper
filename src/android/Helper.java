package com.nokia.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import android.view.ViewGroup;

public class Helper extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("sendToBack")) {

            webView.getView().setBackgroundColor(0x00000000);
            ((ViewGroup)webView.getView()).bringToFront();

            return true;

        } else {
            
            return false;

        }
    }
}
